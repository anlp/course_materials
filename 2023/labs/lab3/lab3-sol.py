from __future__ import division
import sys
from pprint import pprint
from collections import defaultdict
import nltk
from nltk.corpus import treebank
from nltk import ConditionalFreqDist, Nonterminal, FreqDist
from lab3_helper import *
from BetterICP import *
from nltk import InsideChartParser


## Main body of code ##
# Extracting tagged sentences using NLTK libraries
psents = treebank.parsed_sents()

def parse_to_file(parser,in_name,out_name):
    """Reads sentences from file in_name, parses them with parser
    and writes the parse trees into a file named out_name

    :type parser: BetterICP
    :parm parser: the parser
    :type in_name: str
    :parm in_name: name of file from which to read strings to parse
    :type out_name: str
    :parm out_name: name of file to which to write result trees"""
    input = open(in_name,"r")
    output = open(out_name,"w")
    for sent in input:
        output.write("Parser output for the sentence : "+sent+"\n")
        trees = parser.nbest_parse(sent.split())
        # pprint converts tree into string format so that we can write
        #  it into a file.
        for tree in trees:
            output.write(tree.pprint()+"\n\n")
    output.close()

grammar = parse_pgrammar("""
    # Grammatical productions.
     S -> NP VP [1.0]
     NP -> Pro [0.1] | Det N [0.3] | N [0.59] | NP PP [0.01]
     VP -> Vi [0.05] | Vt NP [0.9] | VP PP [0.05]
     Det -> Art [1.0]
     PP -> Prep NP [1.0]
   # Lexical productions.
     Pro -> "i" [0.3] | "we" [0.1] | "you" [0.1] | "he" [0.3] | "she" [0.2]
     Art -> "a" [0.4] | "an" [0.1] | "the" [0.5]
     Prep -> "with" [0.7] | "in" [0.3]
     N -> "salad" [0.4] | "fork" [0.3] | "mushrooms" [0.3]
     Vi -> "sneezed" [0.5] | "ran" [0.5]
     Vt -> "eat" [0.2] | "eats" [0.2] | "ate" [0.2] | "see" [0.2] | "saw" [0.2]
    """)

sentence1 = "he ate salad"
sentence2 = "he ate salad with mushrooms"
sentence3 = "he ate salad with a fork"

sppc=BetterICP(grammar)
sppc.parse(sentence1.split())
sppc.parse("he sneezed".split())
sppc.parse("he sneezed the fork".split())

sppc.parse(sentence2.split())
sppc.parse(sentence3.split())

#prods = get_costed_productions(psents)
#ppg=PCFG(Nonterminal('NP'), prods)
#ppc=BetterICP(ppg,1000)
#print "beam = 1000"
#ppc.parse("the men".split(),True,3)
#ppc.beam(1075)
#print "beam = 1075"
#ppc.parse("the men".split(),True,3)
#ppc.trace(3)
#ppc.beam(1200)
#print "beam = 1200"
#ppc.parse("the men".split(),True,3)
#ppc.trace(1)
#ppc.beam(1950)
#print "beam = 1950"
#ppc.parse("the men".split(),True,3)

#parser = BetterICP(grammar)
#parse_to_file(parser,"input1.txt","output1.txt");
